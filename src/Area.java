/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0x4ea5
 */
public class Area {
    int width;
    int length;
    Point point; 
    
    public Area()
    {
        point = new Point();
    } 
    
    public int  getWidth() {return width;}
    public void setWidth(int width){this.width = width;}
    
    public int getLength(){return length;}
    public void setLength(int length){this.length = length;}
    
    public int getX(){return point.getX();}
    public void setX(int x){point.setX( x);}
    
    public int getY(){return point.getY();}
    public void setY(int y){point.setY(y);}

}
