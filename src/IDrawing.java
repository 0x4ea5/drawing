/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0x4ea5
 */
public interface IDrawing {

    /**
     *
     * @param img
     * @param p
     * @param imgArea
     */
    public void drawIImage( IImage img, Point p, Area imgArea);
    
    public void drawText(String text, Point p, Pen pen);
    
    public void drawRetangle(Area area, Pen pen);
    
    public void drawCircle(Point p, Pen pen);
    
    public void drawLine(Point src, Point dst, Pen pen);
}
