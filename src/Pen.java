/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0x4ea5
 */
public class Pen {
    int size;
    Color color;
    boolean filled;
    Font font;    
    
    public int getSize(){return size;}
    public void setSize(int size){this.size = size;}
    
    public Color getColor(){return this.color;}
    public void setColor(Color color){this.color = color;}
    
    public void setFilled(boolean f){this.filled = f;}
    public boolean getFilled(){return filled;}
    
    public Font getFont(){return font;}
    public void setFont(Font f){this.font = f;}
}
