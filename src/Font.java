/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 0x4ea5
 */
public class Font {
    public enum FontType{ DEFAULT, 微软雅黑, 宋体 };
    public enum Format{NORMAL, BOLD, ITALIC,};
    public enum Justify{LEFT, CENTER, RIGHT};
    
    FontType font;
    Format format;
    Justify justify;
    int fontSize;
    
    public FontType getFont(){return font;}
    public void setFont(FontType type){ this.font = type;}
    
    public Format getFormat(){ return format;}
    public void setFormat(Format format){this.format = format;}
    
    public Justify getJustify(){    return justify;    }
    public void setJustify(Justify justify){    this.justify = justify; }
    
    public int getFontSize(){return fontSize;}
    public void setFontSIze(int size){this.fontSize = size;}
}
